from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('category/<slug:category_slug>/', views.home, name='products_by_category'),
    path('category/<slug:category_slug>/<slug:product_slug>/', views.product_detail, name='product_detail'),
    path('search/', views.search_view, name='search'),
    path('cart/', views.cart_detail, name='cart_detail'),
    path('cart/add/<int:product_id>/', views.add_cart, name='add_cart'),
    path('cart/remove/<int:product_id>/', views.cart_remove, name='remove_cart'),
    path('cart/delete/<int:product_id>/', views.cart_remove, name='delete_cart'),
    path('account/create/', views.sign_up_view, name='sign_up'),
    path('account/login/', views.login_view, name='log_in'),
    path('account/logout/', views.login_view, name='log_out'),
    path('about/', views.about, name='about'),
    path('contacts/', views.contacts, name='contacts'),
]
